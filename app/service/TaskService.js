'use strict';

angular.module('myApp.taskService', ['ngRoute'])
    .service('TaskService', ['$http', '$rootScope', 'AuthService', function ($http, $rootScope, AuthService) {
        var URL = "http://localhost:8081";

        this.getAllTasks = getAllTasks;
        this.findById = findById;

        function getAllTasks() {
            $http.get(URL + '/task/list').then(
                function (odpowiedz) {
                    $rootScope.tasks = [];
                    for (var index in odpowiedz.data) {
                        var task = odpowiedz.data[index];
                        $rootScope.tasks.push(task);
                    }
                    console.log(odpowiedz.data);
                }, function (odpowiedzBlad) {
                    console.log(odpowiedzBlad);
                });
        }

        function findById(id) {
            for (var i = 0; i < $rootScope.tasks.length; i++) {
                if ($rootScope.tasks[i] === id) {
                    return $rootScope.tasks[i];
                }
            }
        }

    }]);

