'use strict';

angular.module('myApp.addTask', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/addTask', {
            templateUrl: 'addTask/addTask.html',
            controller: 'AddTaskCtrl'
        });
    }])

    .controller('AddTaskCtrl', ['TaskService', '$http', function (TaskService, $http) {
        var URL = "http://localhost:8081";
        var self = this;

        this.taskAdd = {
            'title': '',
            'description': '',
            'dueDate': ''

        };

        this.addTask = function () {
            $http.post(URL + '/task/create', self.taskAdd).then(
                function (odpowiedz) {
                    console.log(odpowiedz);
                    location.reload();
                },
                function (odpowiedzNaBlad) {
                    console.log(odpowiedzNaBlad);
                });
        };
    }]);