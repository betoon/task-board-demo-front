'use strict';

angular.module('myApp.viewTask', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewTask', {
            templateUrl: 'viewTask/viewTask.html',
            controller: 'ViewTaskCtrl'
        });
    }])

    .controller('ViewTaskCtrl', ['TaskService', function (TaskService) {
        this.showSingleTask = function () {
            TaskService.findById(id);
        };
        this.showSingleTask();
    }]);