'use strict';

angular.module('myApp.viewUsersList', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/viewUsersList', {
            templateUrl: 'viewUsersList/viewUsersList.html',
            controller: 'ViewUsersListCtrl'
        });
    }])

    .controller('ViewUsersListCtrl', ['UserService', '$rootScope', function(UserService, $rootScope) {
        this.showAllUsers = function () {
            UserService.getAllUsers();
        };
        this.showAllUsers();

        $rootScope.showSelectedUser = function (user) {
            $rootScope.selectedUser = user;
        };

    }]);