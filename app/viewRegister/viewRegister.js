'use strict';

angular.module('myApp.viewRegister', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewRegister', {
            templateUrl: 'viewRegister/viewRegister.html',
            controller: 'ViewRegisterCtrl'
        });
    }])

    .controller('ViewRegisterCtrl', ['$http', '$routeParams', '$window', 'AuthService', function ($http, $routeParams, $window, AuthService) {
        var URL = "http://localhost:8081";
        var self = this;

        this.registerUser = {
            'login': '',
            'email': '',
            'password': ''
        };

        this.sendRegisterForm = function () {
            $http.post(URL + '/auth/register', self.registerUser).then(
                function (odpowiedz) {
                    console.log(odpowiedz)
                },
                function (odpowiedzNaBlad) {
                    console.log(odpowiedzNaBlad)

                }
            );

        };

    }]);