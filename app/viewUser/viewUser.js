'use strict';

angular.module('myApp.viewUser', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/viewUser', {
            templateUrl: 'viewUser/viewUser.html',
            controller: 'ViewUserCtrl'
        });
    }])

    .controller('ViewUserCtrl', [function() {

    }]);