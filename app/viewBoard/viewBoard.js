'use strict';

angular.module('myApp.viewBoard', ['ngRoute'])

    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/viewBoard', {
            templateUrl: 'viewBoard/viewBoard.html',
            controller: 'ViewBoardCtrl'
        });
    }])

    .controller('ViewBoardCtrl', [function() {

    }]);