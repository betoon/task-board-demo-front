'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.view1',
    'myApp.view2',
    'myApp.viewTask',
    'myApp.addTask',
    'myApp.viewTasksList',
    'myApp.viewUsersList',
    'myApp.viewLogin',
    'myApp.authService',
    'myApp.taskService',
    'myApp.userService',
    'myApp.viewRegister',
    'myApp.viewUser',
    'myApp.viewBoard',
    'myApp.version',
    'ui.router'
]).config(['$locationProvider', '$routeProvider', '$stateProvider',
    function ($locationProvider, $routeProvider, $stateProvider) {
        $locationProvider.hashPrefix('!');

        $routeProvider.otherwise({redirectTo: '/viewLogin'});

        $stateProvider.state('viewLogin', {
            url: '/viewLogin',
            templateUrl: 'viewLogin/viewLogin.html',
            controller: 'ViewLoginCtrl'
        })
            .state('viewRegister', {
                url: '/viewRegister',
                templateUrl: 'viewRegister/viewRegister.html',
                controller: 'ViewRegisterCtrl'
            })
            .state('viewTasksList.html', {
                url: '/viewTasksList',
                templateUrl: 'viewTasksList/viewTasksList.html',
                controller: 'ViewTasksListCtrl'
            })
            .state('addTask.html', {
                url: '/addTask',
                templateUrl: 'addTask/addTask.html',
                controller: 'AddTaskCtrl'
            })
            .state('viewUsersList.html', {
                url: '/viewUsersList',
                templateUrl: 'viewUsersList/viewUsersList.html',
                controller: 'ViewUsersListCtrl'
            })


    }]).run(function (AuthService, $http, $window, $rootScope) {

        //dodac logout
    $rootScope.logout = function () {
        $window.sessionStorage.removeItem('token');
        $window.sessionStorage.removeItem('user_id');
        AuthService.loggedInUser.id = '';
    };

    if (AuthService.loggedInUser.id === '') {
        console.log('redirect');
        // jesteśmy niezalogowani
        var token = $window.sessionStorage.getItem('token');
        var user_id = $window.sessionStorage.getItem('user_id');

        if (token === null || token === undefined || user_id === null || user_id === undefined) {
            // to znaczy ze nie mamy danych logowania w sesji
            $window.location = "#!/viewLogin";
        } else {
            $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;
            AuthService.loggedInUser.id = user_id;
        }
    }

}).run(function ($rootScope, $window, AuthService, $location) {
    $rootScope.$on('$routeChangeStart', function ($event, next, current) {
        if (next.originalPath !== '/viewLogin' && next.originalPath !== '/viewRegister') {
            console.log(AuthService.loggedInUser.id);
            if (AuthService.loggedInUser.id === '') {
                $location.path('/viewLogin');
            }
        }
    });
});



//run powyzej, ze zdarzenia mozemy wyciagnac nastepny stan(tj strona) i mozemy wywolac sprawdzenie || if(AuthServ... ||
