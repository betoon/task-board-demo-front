'use strict';

angular.module('myApp.viewTasksList', ['ngRoute'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/viewTasksList', {
            templateUrl: 'viewTasksList/viewTasksList.html',
            controller: 'ViewTasksListCtrl'
        });
    }])

    .controller('ViewTasksListCtrl', ['AuthService', '$http', 'TaskService', '$rootScope', function (AuthService, $http, TaskService, $rootScope) {
        var URL = 'http://localhost:8081';
        var self = this;

        this.atributesToAssign = {
            'taskStatuses.id': 2
        };

        this.showAllTasks = function () {
            TaskService.getAllTasks();
        };
        this.showAllTasks();

        $rootScope.showSelectedTask = function (task) {
            $rootScope.selectedTask = task;
        };

        this.removeSelectedTask = function (id) {
            $http.delete(URL + '/task/remove?id=' + id)
                .then(function (odpowiedz) {
                    console.log(odpowiedz);
                    location.reload();
                }, function (odpowiedzBlad) {
                    console.log(odpowiedzBlad);
                })
        };

        this.assignLoggedInUser = function (taskId) {

            $http.put(URL + '/task/assign?id=' + taskId + '&user=' + AuthService.loggedInUser.id, self.atributesToAssign)
                .then(function (odpowiedz) {
                    console.log(odpowiedz);
                    location.reload();
                }, function (odpowiedzBlad) {
                    console.log(odpowiedzBlad);
                })
        };
    }]);